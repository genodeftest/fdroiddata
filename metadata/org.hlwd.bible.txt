Categories:Reading
License:GPL-3.0
Web Site:https://gitlab.com/hotlittlewhitedog/BibleMultiTheLight/blob/HEAD/README.md
Source Code:https://gitlab.com/hotlittlewhitedog/BibleMultiTheLight/tree/HEAD
Issue Tracker:https://gitlab.com/hotlittlewhitedog/BibleMultiTheLight/issues

Auto Name:The Light
Summary:Bible multi languages, free, offline, no advertising
Description:
Bible multi languages, free, offline, no advertising, completely in English,
French, Italian, Spanish.

* King James Version, Segond, Diodati, Valera.
* Easy to use with quick search and share, plans of reading, articles.
.

Repo Type:git
Repo:https://gitlab.com/hotlittlewhitedog/BibleMultiTheLight.git

Build:2.8,20171010
    commit=0a8efec9fc6a57e13924aac289aefd8da2f23f99
    subdir=app
    gradle=yes

Build:2.9,20171212
    commit=3f23d351ecc3281ccfe744a0372391e43cc6d65e
    subdir=app
    gradle=yes

Build:2.10,20180107
    commit=a38c666f2ec6742ef976dcdfa14652a41dcbc75c
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:None
Current Version:2.10
Current Version Code:20180107
